﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemorialCards
{
    class MemorialCardsModel
    {

        public Int64 document_number { get; set; }
        public Int64 account_number { get; set; }
        public Int64 response_id { get; set; }
        public string from_name { get; set; }
        public string to_name { get; set; }
        public string message { get; set; }
        public bool send_donor { get; set; }
        public string address { get; set; }
    }
}
