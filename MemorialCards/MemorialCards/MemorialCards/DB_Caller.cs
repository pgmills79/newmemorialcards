﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using System.Data;

namespace MemorialCards
{
    class DB_Caller
    {

        public List<MemorialCardsModel> GetMemorialCardList()
        {
            SE_Entities db = new SE_Entities();

           // IBind

           var cards = from da in db.T29_DonationAcknowledgment
                        join am in db.A03_AddressMaster
                            on da.RecipientAddressId equals am.AddressId
                        join aa in db.A02_AccountAddresses
                            on da.RecipientAccountNumber equals aa.AccountNumber
                        where aa.Active == true
                          && aa.UseAsPrimary == true
                          && am.Type == "MAIL"
                        orderby da.DocumentNumber
                        select  new MemorialCardsModel
                        {
                            document_number = da.DocumentNumber,
                            account_number = da.RecipientAccountNumber,
                            response_id = da.ResponseId,
                            from_name = da.FromName,
                            to_name = da.ToName,
                            message = da.Message,
                            send_donor = da.SendToDonor,
                            address = am.AddressLine1
                         };

            //IEnumerable<DataRow>() list_cards = cards.AsEnumerable<DataRow>();

            return cards.ToList<MemorialCardsModel>();   
                                  

        }

        public List<MemorialCardsModel> GetMemorialCardListByAccountNumber(string account_number)
        {
            SE_Entities db = new SE_Entities();

            // IBind

            var cards = from da in db.T29_DonationAcknowledgment
                        join am in db.A03_AddressMaster
                            on da.RecipientAddressId equals am.AddressId
                        join aa in db.A02_AccountAddresses
                            on da.RecipientAccountNumber equals aa.AccountNumber
                        where aa.Active == true
                          && aa.UseAsPrimary == true
                          && am.Type == "MAIL"
                          && aa.AccountNumber.ToString().Contains(account_number)
                        orderby da.DocumentNumber
                        select new MemorialCardsModel
                        {
                            document_number = da.DocumentNumber,
                            account_number = da.RecipientAccountNumber,
                            response_id = da.ResponseId,
                            from_name = da.FromName,
                            to_name = da.ToName,
                            message = da.Message,
                            send_donor = da.SendToDonor,
                            address = am.AddressLine1
                        };

            //IEnumerable<DataRow>() list_cards = cards.AsEnumerable<DataRow>();

            return cards.ToList<MemorialCardsModel>();


        }

        public List<Int64> GetAccountNumbers(string contains_text = "")
        {
            SE_Entities db = new SE_Entities();

            // IBind

            List<Int64> accounts = db.T29_DonationAcknowledgment
                                    .Where(a => a.RecipientAccountNumber.ToString().Contains(contains_text))
                                    .GroupBy(g => new
                                                    {
                                                        g.RecipientAccountNumber
                                                    }
                                            )
                                    .Select(group => group.Key.RecipientAccountNumber)
                                    .ToList<Int64>();



            //IEnumerable<DataRow>() list_cards = cards.AsEnumerable<DataRow>();

            return accounts;


        }

        public String GetEarliestTransactionDate()
        {
            SE_Entities db = new SE_Entities();

            DateTime min_date = 
                Convert.ToDateTime(
                                    (from da in db.T29_DonationAcknowledgment
                                    join tm in db.T01_TransactionMaster
                                    on da.DocumentNumber equals tm.DocumentNumber
                                    select tm.Date).Min()
                                   );

            return min_date.ToString("MM-dd-yyyy");
                      

        }

        public String GetMostRecentTransactionDate()
        {
            SE_Entities db = new SE_Entities();

            DateTime max_date =
                Convert.ToDateTime(
                                    (from da in db.T29_DonationAcknowledgment
                                     join tm in db.T01_TransactionMaster
                                     on da.DocumentNumber equals tm.DocumentNumber
                                     select tm.Date).Max()
                                   );

            return max_date.ToString("MM-dd-yyyy");


        }

       
    }
}
