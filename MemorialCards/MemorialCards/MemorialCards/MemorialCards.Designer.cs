﻿namespace MemorialCards
{
    partial class frmMemorialCards
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMemorialCards = new System.Windows.Forms.DataGridView();
            this.tbAccountNumbers = new System.Windows.Forms.TextBox();
            this.lblEnterAccountNumber = new System.Windows.Forms.Label();
            this.mtbBeginDate = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mtbEndDate = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMemorialCards)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMemorialCards
            // 
            this.dgvMemorialCards.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMemorialCards.Location = new System.Drawing.Point(12, 206);
            this.dgvMemorialCards.Name = "dgvMemorialCards";
            this.dgvMemorialCards.Size = new System.Drawing.Size(790, 219);
            this.dgvMemorialCards.TabIndex = 0;
            this.dgvMemorialCards.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvMemorialCards_ColumnHeaderMouseClick);
            // 
            // tbAccountNumbers
            // 
            this.tbAccountNumbers.Location = new System.Drawing.Point(153, 66);
            this.tbAccountNumbers.Name = "tbAccountNumbers";
            this.tbAccountNumbers.Size = new System.Drawing.Size(136, 20);
            this.tbAccountNumbers.TabIndex = 1;
            this.tbAccountNumbers.TextChanged += new System.EventHandler(this.tbAccountNumbers_TextChanged);
            this.tbAccountNumbers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbAccountNumbers_KeyDown);
            this.tbAccountNumbers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAccountNumbers_KeyPress);
            // 
            // lblEnterAccountNumber
            // 
            this.lblEnterAccountNumber.AutoSize = true;
            this.lblEnterAccountNumber.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnterAccountNumber.Location = new System.Drawing.Point(24, 70);
            this.lblEnterAccountNumber.Name = "lblEnterAccountNumber";
            this.lblEnterAccountNumber.Size = new System.Drawing.Size(123, 16);
            this.lblEnterAccountNumber.TabIndex = 2;
            this.lblEnterAccountNumber.Text = "Account Number:";
            // 
            // mtbBeginDate
            // 
            this.mtbBeginDate.Location = new System.Drawing.Point(153, 96);
            this.mtbBeginDate.Mask = "00/00/0000";
            this.mtbBeginDate.Name = "mtbBeginDate";
            this.mtbBeginDate.Size = new System.Drawing.Size(136, 20);
            this.mtbBeginDate.TabIndex = 5;
            this.mtbBeginDate.ValidatingType = typeof(System.DateTime);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Begin Date:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // mtbEndDate
            // 
            this.mtbEndDate.Location = new System.Drawing.Point(153, 126);
            this.mtbEndDate.Mask = "00/00/0000";
            this.mtbEndDate.Name = "mtbEndDate";
            this.mtbEndDate.Size = new System.Drawing.Size(136, 20);
            this.mtbEndDate.TabIndex = 7;
            this.mtbEndDate.ValidatingType = typeof(System.DateTime);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "End Date:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // frmMemorialCards
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 437);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.mtbEndDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.mtbBeginDate);
            this.Controls.Add(this.lblEnterAccountNumber);
            this.Controls.Add(this.tbAccountNumbers);
            this.Controls.Add(this.dgvMemorialCards);
            this.Name = "frmMemorialCards";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Memorial Cards";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMemorialCards)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMemorialCards;
        private System.Windows.Forms.TextBox tbAccountNumbers;
        private System.Windows.Forms.Label lblEnterAccountNumber;
        private System.Windows.Forms.MaskedTextBox mtbBeginDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox mtbEndDate;
        private System.Windows.Forms.Label label3;
    }
}

