﻿using Equin.ApplicationFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MemorialCards
{
    public partial class frmMemorialCards : Form
    {
        public frmMemorialCards()
        {
            InitializeComponent();
        }

        public StringBuilder account_filter { get; set; }

        private void Form1_Load(object sender, EventArgs e)
        {
            //load memorial cards
            DB_Caller db = new DB_Caller();
            account_filter = new StringBuilder();

            mtbBeginDate.Text = db.GetEarliestTransactionDate();
            mtbEndDate.Text = db.GetMostRecentTransactionDate();          
            

            //we have to bind the enity<> object to this extension so we can sort
            BindingListView<MemorialCardsModel> view = 
                new BindingListView<MemorialCardsModel>(db.GetMemorialCardList());
                    
            dgvMemorialCards.DataSource = view;         

            foreach (DataGridViewColumn column in dgvMemorialCards.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }

           

        }

        private void dgvMemorialCards_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
            //we get currently sorted column (if there are no sorted columns it will be null)
            DataGridViewColumn cur_sorted_column = dgvMemorialCards.SortedColumn;

            SortOrder cur_direction = dgvMemorialCards.SortOrder;           

            if (dgvMemorialCards.Columns[e.ColumnIndex].Equals(cur_sorted_column))
            {
              dgvMemorialCards.Sort(dgvMemorialCards.Columns[e.ColumnIndex],
                    cur_direction == SortOrder.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending);
                
            }
            else
            {
                dgvMemorialCards.Sort(dgvMemorialCards.Columns[e.ColumnIndex],
                ListSortDirection.Ascending);
            }

        }        

       

        private void tbAccountNumbers_KeyDown(object sender, KeyEventArgs e)
        {
            //e.SuppressKeyPress = !char.IsDigit(e.key);
            ////here we will filter by key value

            //if (!e.SuppressKeyPress)
            //{
            //    var test = GetNumber(e.KeyCode.ToString());
            //}
           
            
        }

       

        private string GetNumber(string keyvalue)
        {
            string key = string.Empty;
            switch (keyvalue)
            {
                case "D1":
                    key = "1";
                    break;
                case "D2":
                    key =  "2";
                    break;
                case "D3":
                    key =  "3";
                    break;
                case "D4":
                    key = "4";
                    break;
                case "D5":
                    key = "5";
                    break;
                case "D6":
                    key = "6";
                    break;
                case "D7":
                    key = "7";
                    break;
                case "D8":
                    key = "8";
                    break;
                case "D9":
                    key = "9";
                    break;
                case "NumPad1":
                    key = "1";
                    break;
                case "NumPad2":
                    key = "2";
                    break;
                case "NumPad3":
                    key = "3";
                    break;
                case "NumPad4":
                    key = "4";
                    break;
                case "NumPad5":
                    key = "5";
                    break;
                case "NumPad6":
                    key = "6";
                    break;
                case "NumPad7":
                    key = "7";
                    break;
                case "NumPad8":
                    key = "8";
                    break;
                case "NumPad9":
                    key = "9";
                    break;
                default:
                    break;
            }

            return key;

        }

        private void tbAccountNumbers_KeyPress(object sender, KeyPressEventArgs e)
        {

            string keyvalue = e.KeyChar.ToString();
            DB_Caller db = new DB_Caller();

            if (Char.IsDigit(e.KeyChar))
            {
                //here we want to add to filter
                account_filter.Append(keyvalue);
                

                BindingListView<MemorialCardsModel> view =
                new BindingListView<MemorialCardsModel>(db.GetMemorialCardListByAccountNumber(account_filter.ToString()));

                dgvMemorialCards.DataSource = view;

                foreach (DataGridViewColumn column in dgvMemorialCards.Columns)
                {
                    column.SortMode = DataGridViewColumnSortMode.Programmatic;
                }

                return;
            }
            if (Char.IsControl(e.KeyChar))
            {
                //this means backspace and there was a digit removed 
                if (tbAccountNumbers.TextLength > 0
                    && e.KeyChar.ToString().Equals("\b"))
                {
                    account_filter.Remove(tbAccountNumbers.TextLength - 1, 1);
                    if (account_filter.Length.Equals(0))
                    {
                        BindingListView<MemorialCardsModel> view =
                            new BindingListView<MemorialCardsModel>(db.GetMemorialCardList());

                        dgvMemorialCards.DataSource = view;

                        foreach (DataGridViewColumn column in dgvMemorialCards.Columns)
                        {
                            column.SortMode = DataGridViewColumnSortMode.Programmatic;
                        }
                    }
                    else
                    {
                        BindingListView<MemorialCardsModel> view =
                            new BindingListView<MemorialCardsModel>(db.GetMemorialCardListByAccountNumber(account_filter.ToString()));

                        dgvMemorialCards.DataSource = view;

                        foreach (DataGridViewColumn column in dgvMemorialCards.Columns)
                        {
                            column.SortMode = DataGridViewColumnSortMode.Programmatic;
                        }
                    }
               }

                return;
            }

               
            e.Handled = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tbAccountNumbers_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
